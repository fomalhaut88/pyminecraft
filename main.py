from pyminecraft.core.sender import Sender
from pyminecraft.blocks import Air, Stone, CraftingTable, Torch, RedstoneTorch, UnpoweredRepeater, RedstoneWire
from pyminecraft.structures import Struct, StructFromFile
from pyminecraft.transforms import Translate, Rotate


if __name__ == "__main__":
    # s = Column(-110, 88, 277, 0, 5)
    s = StructFromFile(-110, 88, 277, 0, '/home/alexander/Desktop/garden.mcstr')

    # 54 69 535
    blocks = []
    blocks.append(Stone(0, 0, 0))
    blocks.append(Stone(1, 0, 0))
    blocks.append(Stone(2, 0, 0))
    blocks.append(RedstoneWire(0, 1, 0))
    blocks.append(UnpoweredRepeater(1, 1, 0, 1))
    blocks.append(RedstoneTorch(3, 0, 0, 0, up=0))

    rot = Rotate(3)
    for b in blocks:
        rot.apply(b)

    trans = Translate(54, 69, 535)
    for b in blocks:
        trans.apply(b)

    sender = Sender('127.0.0.1', 9034)
    for block in blocks:
        sender.send_block(block)
