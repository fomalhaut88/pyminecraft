"""
To make release:
    python3 setup.py sdist

Installation:
    python3 setup.py install
"""

from distutils.core import setup
from setuptools import find_packages

setup(
    name='Pyminecraft',
    version='dev',
    packages=find_packages(),
    license='Free',
    long_description=open('README.md').read()
)
