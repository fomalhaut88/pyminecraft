import re


def camel2lower(s):
    s = re.sub(r'([A-Z])', r'_\1', s).lower()
    if s.startswith('_'):
        s = s[1:]
    return s


def lower2camel(s):
    return ''.join(e.title() for e in s.split('_'))
