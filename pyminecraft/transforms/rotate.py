from .transform import Transform


class Rotate(Transform):
    def __init__(self, angle):
        self.angle = angle % 4

    def apply(self, block):
        if self.angle == 1:
            block.x, block.z = block.z, -block.x
        elif self.angle == 2:
            block.x, block.z = -block.x, -block.z
        elif self.angle == 3:
            block.x, block.z = -block.z, block.x

        if hasattr(block, 'd'):
            block.d = (block.d + self.angle) % 4

    def __repr__(self):
        return "Rotate(angle={})".format(self.angle)
