from .transform import Transform


class Translate(Transform):
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def apply(self, block):
        block.x += self.x
        block.y += self.y
        block.z += self.z

    def __repr__(self):
        return "Translate(x={}, y={}, z={})".format(self.x, self.y, self.z)
