class Transform:
    def apply(self, block):
        raise NotImplementedError()

    def apply_many(self, blocks):
        for block in blocks:
            self.apply(block)
