import socket
from time import sleep


TIMEOUT = 0.01


class Sender:
    def __init__(self, host, port):
        self.host = host
        self.port = port

    def send(self, msg):
        body = str.encode(msg)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.host, self.port))
        sock.send(body)
        sock.close()
        sleep(TIMEOUT)

    def send_block(self, block):
        msg = 'setblock {x} {y} {z} {name} {data} replace'.format(
            x=block.x,
            y=block.y,
            z=block.z,
            name=block.name,
            data=block.data
        )
        self.send(msg)

    def send_struct(self, struct):
        for block in struct.blocks:
            self.send_block(block)

    def remove_block(self, block):
        msg = 'setblock {x} {y} {z} air 0 replace'.format(
            x=block.x,
            y=block.y,
            z=block.z
        )
        self.send(msg)

    def remove_struct(self, struct):
        for block in struct.blocks:
            self.remove_block(block)

    def remove_area(self, coord1, coord2):
        number = (abs(coord1[0] - coord2[0]) + 1) * \
            (abs(coord1[1] - coord2[1]) + 1) * \
            (abs(coord1[2] - coord2[2]) + 1)

        if number > 32768:
            raise ValueError("Too many blocks to remove: {} > 32768".format(number))

        msg = 'fill {x1} {y1} {z1} {x2} {y2} {z2} air'.format(
            x1=coord1[0],
            y1=coord1[1],
            z1=coord1[2],
            x2=coord2[0],
            y2=coord2[1],
            z2=coord2[2]
        )
        self.send(msg)
