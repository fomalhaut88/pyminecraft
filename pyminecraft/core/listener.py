import sys
import socket


MSG_SIZE = 1024


class Listener:
    def __init__(self, host, port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((host, port))
        self.sock.listen(1)

    def run(self):
        while True:
            try:
                connection, addr = self.sock.accept()
                body = connection.recv(MSG_SIZE)
                connection.close()

                msg = body.decode('utf-8')
                print(msg)
                sys.stdout.flush()
            except KeyboardInterrupt:
                break
