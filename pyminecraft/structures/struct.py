from pyminecraft.transforms import Translate, Rotate


class Struct:
    def __init__(self, x, y, z, d):
        self.blocks = []
        self.init_blocks()
        Rotate(d).apply_many(self.blocks)
        Translate(x, y, z).apply_many(self.blocks)

    def init_blocks(self):
        pass
