from pyminecraft import blocks, utils
from .struct import Struct


class StructFromFile(Struct):
    def __init__(self, x, y, z, d, file_path, without_air=True):
        self.file_path = file_path
        self.without_air = without_air
        super().__init__(x, y, z, d)

    def init_blocks(self):
        materials, content = self._parse_file()

        for yi, content_row in enumerate(content):
            for zi, x_row in enumerate(content_row):
                for xi, num in enumerate(x_row):
                    block_cls = materials[num]['block_cls']
                    params = materials[num]['params']
                    block = block_cls(xi, yi, zi, *params)
                    self.blocks.append(block)

        self._delete_air()

        self.blocks.sort(
            key=lambda block: isinstance(block, blocks.HangingBlock)
        )

    def _parse_file(self):
        with open(self.file_path) as f:
            h = -1
            materials = {}
            content = []
            content_row = []

            for line in f:
                row = line.strip().split()

                if not row:
                    if h >= 0:
                        content.append(content_row)
                        content_row = []
                    h += 1
                    continue

                if h < 0:
                    block_cls_name = utils.lower2camel(row[1])
                    block_cls = getattr(blocks, block_cls_name)
                    params = list(map(int, row[2:]))

                    materials[row[0]] = {
                        "block_cls": block_cls,
                        "params": params
                    }

                else:
                    content_row.append(row)

        content.append(content_row)

        return materials, content

    def _delete_air(self):
        if self.without_air:
            for i in range(len(self.blocks) - 1, -1, -1):
                if isinstance(self.blocks[i], blocks.Air):
                    del self.blocks[i]
