from .rotatable_block import RotatableBlock
from .hanging_block import HangingBlock


class WoodenDoor(HangingBlock, RotatableBlock):
    pass
