import os
from pyminecraft import utils

ignored_files = (
    '__init__.py',
    'block.py',
    'rotatable_block.py',
    'typed_block.py',
)
current_dir = os.path.dirname(__file__)

for file_name in os.listdir(current_dir):
    if file_name.endswith('.py') and file_name not in ignored_files:
        name = file_name[:-3]
        cls_name = utils.lower2camel(name)
        exec('from .{} import {}'.format(name, cls_name))
