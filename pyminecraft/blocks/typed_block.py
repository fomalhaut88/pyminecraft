from .block import Block


class TypedBlock(Block):
    def __init__(self, x, y, z, tp=0):
        super().__init__(x, y, z)
        self.tp = tp

    @property
    def data(self):
        return self.tp
