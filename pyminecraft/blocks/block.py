from pyminecraft import utils


class Block:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    @property
    def name(self):
        return utils.camel2lower(self.__class__.__name__)

    @property
    def data(self):
        return 0

    def __repr__(self):
        return "{}(x={}, y={}, z={}, data={})".format(
            self.name,
            self.x, self.y, self.z, self.data
        )


