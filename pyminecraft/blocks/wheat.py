from .typed_block import TypedBlock
from .hanging_block import HangingBlock


class Wheat(HangingBlock, TypedBlock):
    pass
