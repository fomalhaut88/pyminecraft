from .block import Block


class RotatableBlock(Block):
    def __init__(self, x, y, z, d):
        super().__init__(x, y, z)
        self.d = d

    @property
    def data(self):
        return [0, 3, 1, 2][self.d]

