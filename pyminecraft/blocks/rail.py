from .rotatable_block import RotatableBlock


class Rail(RotatableBlock):
    @property
    def data(self):
        return super().data % 2
