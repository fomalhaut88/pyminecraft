from .rotatable_block import RotatableBlock


class Bed(RotatableBlock):
    def __init__(self, x, y, z, d=0, head=0):
        super().__init__(x, y, z, d)
        self.head = head

    @property
    def data(self):
        return 8 * head + super().data

