from .rotatable_block import RotatableBlock


class GoldenRail(RotatableBlock):
    @property
    def data(self):
        return super().data % 2

