from .rotatable_block import RotatableBlock
from .hanging_block import HangingBlock


class WallSign(HangingBlock, RotatableBlock):
    def __init__(self, x, y, z, d=0, up=0, right=0, opn=0):
        super().__init__(x, y, z, d)
        self.up = up
        self.right = right
        self.opn = opn

    @property
    def data(self):
        if self.up:
            return self.right + 8
        else:
            return super().data + 4 * self.opn
