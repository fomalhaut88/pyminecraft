from .rotatable_block import RotatableBlock
from .hanging_block import HangingBlock


class Lever(HangingBlock, RotatableBlock):
    def __init__(self, x, y, z, d=0, a=0):
        super().__init__(x, y, z, d)
        self.a = a

    @property
    def data(self):
        return 8 * self.a + super().data + 1

'''
Bits    Values

0x1
0x2
0x4
A three-bit field storing a value from 0 to 7:

0: Lever on block bottom points east when off.
1: Lever on block side facing east
2: Lever on block side facing west
3: Lever on block side facing south
4: Lever on block side facing north
5: Lever on block top points south when off.
6: Lever on block top points east when off.
7: Lever on block bottom points south when off.

0x8 If this bit is set, the lever is active.

'''
