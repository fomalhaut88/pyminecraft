from .typed_block import TypedBlock
from .hanging_block import HangingBlock


class MelonStem(HangingBlock, TypedBlock):
    pass
