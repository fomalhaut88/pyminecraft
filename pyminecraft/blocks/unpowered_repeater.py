from .rotatable_block import RotatableBlock
from .hanging_block import HangingBlock


class UnpoweredRepeater(HangingBlock, RotatableBlock):
    def __init__(self, x, y, z, d=0, delay=0):
        super().__init__(x, y, z, d)
        self.delay = delay

    @property
    def data(self):
        return 4 * self.delay + [2, 1, 0, 3][self.d]

