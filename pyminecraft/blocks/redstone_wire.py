from .block import Block
from .hanging_block import HangingBlock


class RedstoneWire(HangingBlock, Block):
    pass
