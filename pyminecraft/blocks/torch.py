from .rotatable_block import RotatableBlock
from .hanging_block import HangingBlock


class Torch(HangingBlock, RotatableBlock):
    def __init__(self, x, y, z, d=0, up=1):
        super().__init__(x, y, z, d)
        self.up = up

    @property
    def data(self):
        if self.up:
            return 5
        else:
            return [1, 4, 2, 3][self.d]
