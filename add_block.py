"""
This script generates block class file in 'pyminecraft/blocks'.
Example:
    python3 add_block.py bookshelf
    python3 add_block.py pumpkin -t rotatable
    python3 add_block.py dirt -t typed
"""
import os
from argparse import ArgumentParser

from pyminecraft import utils


BASE_DIR = os.path.dirname(__file__)
BLOCKS_DIR = os.path.join(BASE_DIR, 'pyminecraft/blocks')
CONTENT_TPL = """from .{base_name} import {base_cls}


class {cls}({base_cls}):
    pass
"""


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('name')
    parser.add_argument('-t', '--type', choices=['typed', 'rotatable'])
    args = parser.parse_args()

    file_name = args.name + '.py'
    file_path = os.path.join(BLOCKS_DIR, file_name)
    cls = utils.lower2camel(args.name)

    base_name = 'block'
    if args.type == 'typed':
        base_name = 'typed_block'
    if args.type == 'rotatable':
        base_name = 'rotatable_block'
    base_cls = utils.lower2camel(base_name)

    content = CONTENT_TPL.format(
        cls=cls,
        base_cls=base_cls,
        base_name=base_name
    )

    with open(file_path, 'w') as f:
        f.write(content)
