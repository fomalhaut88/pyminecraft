# Pyminecraft

Pyminecraft is a python library that helps to generate structures in a minecraft world.


## 1. Overview

Pyminecraft is designed to help people to interact with a minecraft world generating as many blocks as you like. Also it is a good way to dive into Python as a programming language because generated structures can be visualized within the minecraft world that makes the process captivating. It works for Windows and Linux as well.


## 2. Getting started

You should follow the instructure below to make it work:


### 2.1 Download Python 3

If you use Linux, python 3 is already installed with the system. For Windows, you should download the installer from the python official website. Use need the version 3 or higher. I would also recommed to add the path to python interpreter to the `path` enviroment variable.

[https://www.python.org/downloads/](https://www.python.org/downloads/)

If you have done everything correctly you can type `python` (or `python3` for Linux) in the terminal and you will see the standard python prompt with its version.


### 2.2 Download Minecraft client

Download minecraft client from any website you like. If you enjoy playing minecraft, I am sure it won't be a problem to find it yourself.


### 2.3 Download Minecraft server

First you need to get known which version of minecraft client you have got installed. Supposing the version is 1.11.2. So you can download the server by this url:

[https://s3.amazonaws.com/Minecraft.Download/versions/1.11.2/minecraft_server.1.11.2.jar](https://s3.amazonaws.com/Minecraft.Download/versions/1.11.2/minecraft_server.1.11.2.jar)

In order to download another version just correct the numbers of version in the link.

Also you should confirm the eula license: open the file `eula.txt` and make sure that there is `eula=true` over there.


### 2.4 Downloading Pyminecraft

Follow this link and download Pyinstaller as a folder:

[https://bitbucket.org/fomalhaut88/pyminecraft/](https://bitbucket.org/fomalhaut88/pyminecraft/)


### 2.5 Install Pyinstaller

Open the terminal inside the folder you just downloaded and execute the following command:

    python setup.py install

If everything is fine, you can execute this command without any errors:

    python -c "import pyminecraft"


### 2.6 Prepare your first project.

Create a folder where you are going to code. Copy the file `launch.py` from pyinstaller into there. Also create another file there called `example.py`.


### 2.7 Start your server

The starting of the server is a little bit untypical, because the server should listen to the commands generated by your code. To make it work you should go into the minecraft server folder and execute this command:

    python /path/to/your/project/launch.py | java -Xmx1024M -Xms1024M -jar minecraft_server.jar nogui


### 2.8 Start your client

Run your minecraft client and choose `Multiplayer`. After that join the server with ip `127.0.0.1`. If everything works fine you will enter the server.


### 2.9 Run your first program

In the project folder open the created file `example.py` and type this code there:

    from pyminecraft.core.sender import Sender
    from pyminecraft.blocks import Stone

    sender = Sender('127.0.0.1', 9034)
    block = Stone(-110, 88, 277)
    sender.send_block(block)

Execute the code by running:

    python example.py

If you do everything correctly, the stone block will appear in the coordinates (-110, 88, 277). If you want to generate the block at another place, change the coordinates. To find out your currect coordinates in minecraft world use F3 at the client.
